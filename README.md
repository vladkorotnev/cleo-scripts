# CLEO Scripts for GTA Vice City

Most of them were written back in 2013 or so, thus I don't remember what they do or how they work mostly :D

You can compile this with Sanny Builder as usual, but binaries are also included.

## SAS-Akbar.cs

Randomly explodes a car you get into (unless it's a taxi). 40% probability provided that the GTA random generator is a normally distributed one.

*Activation:* None

*Controls:* None

## SAS-Camera.cs

Gives you a camera whenever you die or whatever and loads it for 10000 frames. Useful when you have a camera image saving mod and just love to take pictures of everything.  

*Activation:* None

*Controls:* None

## SAS-CarDoor.cs

Various actions while in car.

*Activation:* None

*Controls:*

* L — lock car door
* U — unlock car door
* T — open car trunk
* Y — open car trunk fully
* I — make car immune to everything
* H — Car Assist — place the car on the nearest road

## SAS-Police.cs

A challenge bloodbath minigame, can't remember anything about how it works or how it's activated :/

As far as I remember it creates a whole lot of cops around you with one of them being a boss with 10k ammo 1k armour and 10k HP. Probably impossible to pass this without any cheats or whatever.

*Activation:* maybe 'z' key, not sure

*Controls:* whatever you use in GTA normally.

## SAS-Racing.cs

Some kind of a police chase minigame which I couldn't finish due to some logic or memory management issue.

*Activation:* get into the highlighted taxi which appears after your respawn somewhere on a nearby road. 

*Controls:* usual driving controls, exit the car to stop the game (however that block is commented out)

## SAS-Taxi.cs

A navigation/taxi system to easily go around Vice City with a set of predefined locations. Probably the first ever CLEO script I wrote :D

When you choose an item while not in a car, it will set the mission restart taxi to appear after you are wasted or busted. That taxi will then take you to the selected location. If you are in a car, it will also take you to the selected location right away. 

*Activation:* Press keys I and O simultaneously to open the menu. 

*Controls:* 

* I+O — open menu
* K — previous item
* L — next item
* I — confirm selection

*Locations:*

* Respawn — last place where you were wasted or busted. This is the default setting.
* Escobar Airport
* Docks
* VicePort
* Leaf Links Club
* North Point Mall
* Downtown — somewhere near the house with the three garage doors and a helicopter
* Movie Studio
* Fire dept.
* Police dept.
* Ocean View
* City Dump

## Savefile.cs

Not sure if it was even written by me...

Allows you to bring up the game save window anytime. 

*Activation:* Press O and P keys simultaneously

*Controls:* None

## SAS-Grav.cs

Controls the gravity in game.

*Activation:* None

*Controls:* Number keys from 1 to 9

## SAS-Aim.txt

An unfinished aimbot for Vice City.

----

© akasaka, 2013-2015



