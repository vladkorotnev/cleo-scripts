
{$CLEO .cs}

//-------------MAIN---------------
thread 'PELEU' 

:CARDOOR_10
wait 50 
if 
   Player.Defined($PLAYER_CHAR)
jf @CARDOOR_10
:cardoor_wait_car
wait 10

if 
00E0:   player $PLAYER_CHAR driving 
jf @cardoor_wait_car

03E5: text_box 'CDHLP'

wait 1000

:CARDOOROELUTZ
if 
00E0:   player $PLAYER_CHAR driving 
jf @cardoor_wait_car

wait 10

if  
    05EE:  key_pressed 76  // Key L
then
    03C1: $car = player $PLAYER_CHAR car_no_save 
    020A: set_car $car door_status_to 2  
    03E5: text_box 'CDLOK'
end

if  
    05EE:  key_pressed 85  // Key U
then
    03C1: $car = player $PLAYER_CHAR car_no_save 
    020A: set_car $car door_status_to 1   
    03E5: text_box 'CDULK'
end

if  
    05EE:  key_pressed 84  // Key T
then
    03C1: $car = player $PLAYER_CHAR car_no_save 
    050B: open_trunk_of_car $car
    03E5: text_box 'TRUOP'
end

if  
    05EE:  key_pressed 89  // Key Y
then
    03C1: $car = player $PLAYER_CHAR car_no_save 
    04E1: open_trunk_of_car_fully $car
    03E5: text_box 'TRUOF'
end

if  
    05EE:  key_pressed 73  // Key I
then
    03C1: $car = player $PLAYER_CHAR car_no_save 
   02AC: set_car $car immunities 1 1 1 1 1 
    03E5: text_box 'CNIMU'
end

if
    05EE: key_pressed 72
then
    // car assist
    03C1: $car = player $PLAYER_CHAR car_no_save 
    00AA: store_car $car position_to $px $py $pz 
    04D3: get_nearest_car_path_coords_from $px $py $pz type 0 store_to $px $py $pz 
   00AB: car $car oelutz_to $px $py $pz 
    03E5: text_box 'CASST'
end

jump @CARDOOROELUTZ

end_thread